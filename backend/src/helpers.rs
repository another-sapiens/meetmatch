use crate::errors::ApiError;
use crate::models::schema::administration as admn;
use crate::{api::file_server_api::ROOT_PAGES, errors::ApiResult};

use chrono::{DateTime, Duration, Utc};
use diesel::prelude::*;
use rand::Rng;
use validator::ValidationError;

/// The required length of a new ID-String.
const ID_LEN: u8 = 6;
/// A list of characters that may be used to construct a new ID. Consists of:
/// 1. Lowercase letters
/// 2. Uppercase letters
/// 3. Numbers
const ALPHANUMERIC: [char; 62] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
    'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4',
    '5', '6', '7', '8', '9',
];

/// Generates a random ID of length [`ID_LEN`] that doesn't exist in [`ROOT_PAGES`].
pub fn get_random_id() -> String {
    let mut rng = rand::thread_rng();
    loop {
        let candidate: String = (0..ID_LEN)
            .map(|_| {
                let idx = rng.gen_range(0..ALPHANUMERIC.len());
                ALPHANUMERIC[idx]
            })
            .collect();

        // Make sure this ID is not "about", "index", "contact", "privacy" or anything similar
        if !ROOT_PAGES.contains(&candidate.as_str()) {
            return candidate;
        }
    }
}
/// Generates a random ID, which doesn't already exist within the database or [`ROOT_PAGES`].
pub fn get_unique_id<F: Fn(&str) -> ApiResult<bool>>(id_is_unique: F) -> ApiResult<String> {
    let mut counter: u8 = 0;
    loop {
        // Create random ID
        let id: String = get_random_id();
        if id_is_unique(&id)? {
            // Return ID if it doesn't already exist in [previous_ids]
            return Ok(id);
        } else if counter == 50 {
            // If no ID could be found in a reasonable amount of time, return an error
            return Err(ApiError::Text("Could not generate unique key.".to_string()));
        }
        counter += 1;
    }
}

/// Checks whether the the provided `admin_id` actually is the correct ID for a poll.
pub fn validate_poll_admin(
    poll_id: &str,
    provided_admin_id: &str,
    c: &diesel::PgConnection,
) -> ApiResult<()> {
    // Find the correct `admin_id` for the poll with the given `poll_id`
    let real_admin_id: String = admn::table.find(&poll_id).select(admn::admin_id).first(c)?;

    // Check whether the provided `admin_id` matches the correct one
    match provided_admin_id == real_admin_id {
        true => Ok(()),
        false => Err(ApiError::IdValidation),
    }
}

/// Checks whether the [DateTime<Utc>] is not more than one year into the future.
pub fn validate_datetime(date_time: &DateTime<Utc>) -> Result<(), ValidationError> {
    let one_year = Duration::weeks(53);
    print!("{}  ->  ", date_time);
    if date_time.signed_duration_since(Utc::now()) > one_year {
        let msg = "DateTime is more than one year into the future.";
        println!("{}", &msg);
        return Err(ValidationError::new(msg));
    }
    println!("DateTime is ok :)");
    Ok(())
}

/// Checks each of the [DateTime]s in [Vec<DateTime<Utc>>] using [`validate_datetime`].
pub fn validate_datetime_vec(date_times: &[DateTime<Utc>]) -> Result<(), ValidationError> {
    for date_time in date_times {
        validate_datetime(date_time)?;
    }
    Ok(())
}
