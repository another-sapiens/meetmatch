table! {
    administration (poll_id) {
        poll_id -> Varchar,
        admin_id -> Varchar,
        creation_date -> Timestamptz,
    }
}

table! {
    polls (poll_id) {
        poll_id -> Varchar,
        title -> Varchar,
        instruction -> Varchar,
        datetime_interval -> Int2,
        offered_datetimes -> Array<Timestamptz>,
        deadline -> Timestamptz,
        theme -> Varchar,
    }
}

table! {
    users (poll_id, user_id) {
        poll_id -> Varchar,
        user_id -> Varchar,
        name -> Varchar,
        email -> Nullable<Varchar>,
    }
}

table! {
    votes (poll_id, vote_id) {
        poll_id -> Varchar,
        vote_id -> Varchar,
        voter_id -> Varchar,
        comment -> Nullable<Varchar>,
        availability -> Array<Timestamptz>,
    }
}

joinable!(administration -> polls (poll_id));
joinable!(users -> polls (poll_id));
joinable!(votes -> polls (poll_id));

allow_tables_to_appear_in_same_query!(
    administration,
    polls,
    users,
    votes,
);
