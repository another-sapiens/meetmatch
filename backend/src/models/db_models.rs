//! Models that can be written into the database.

use super::helper_models::*;
use super::schema::{administration, polls, users, votes};

use chrono::{DateTime, Utc};
use rocket::serde::{Deserialize, Serialize};
use rocket_okapi::JsonSchema;
use rocket_sync_db_pools::diesel::{Insertable, Queryable};

#[derive(Serialize, Insertable, Queryable, Identifiable, AsChangeset, JsonSchema)]
#[primary_key(poll_id, user_id)]
#[table_name = "users"]
pub struct User {
    poll_id: String,
    pub user_id: String,
    /// The user's name.
    pub name: String,
    /// An optional email, for receiving notifications.
    pub email: Option<String>,
}
impl User {
    pub fn new(poll_id: &str, user_id: &str, bare_uservote: &BareUserVote) -> Self {
        Self {
            poll_id: poll_id.to_string(),
            user_id: user_id.to_string(),
            name: bare_uservote.name.to_owned(),
            email: bare_uservote.email.to_owned(),
        }
    }
    pub fn new_admin(poll_id: &str, admin_id: &str, admin_email: Option<String>) -> Self {
        Self {
            poll_id: poll_id.to_string(),
            user_id: admin_id.to_string(),
            name: "".to_string(),
            email: admin_email,
        }
    }
}

/// A user's vote on a given poll.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    Insertable,
    Queryable,
    Identifiable,
    AsChangeset,
    JsonSchema,
)]
#[primary_key(poll_id, vote_id)]
#[table_name = "votes"]
pub struct Vote {
    poll_id: String,
    vote_id: String,
    pub voter_id: String,
    /// An optional comment from the user.
    pub comment: Option<String>,
    /// The [DateTime<Utc>]s where the user is free.
    pub availability: Vec<DateTime<Utc>>,
}
impl Vote {
    pub fn new(poll_id: &str, vote_id: &str, user_id: &str, bare_uservote: &BareUserVote) -> Self {
        Self {
            poll_id: poll_id.to_string(),
            vote_id: vote_id.to_string(),
            voter_id: user_id.to_string(),
            comment: bare_uservote.comment.to_owned(),
            availability: bare_uservote.availability.to_owned(),
        }
    }
}

#[derive(Serialize, Insertable, Queryable, JsonSchema)]
#[table_name = "administration"]
#[serde(rename_all = "camelCase")]
pub struct Administration {
    /// An unique string, which is the key used in querying for this poll.
    poll_id: String,
    /// The user-key of the poll's creator.
    /// Can be used to edit the poll, view its results, and delete the poll.
    pub admin_id: String,
    /// The poll's creation-date. Used to delete the poll after a certain period of time.
    #[serde(skip)]
    creation_date: DateTime<Utc>,
}
impl Administration {
    pub fn new(poll_id: &str, admin_id: &str) -> Self {
        Self {
            poll_id: poll_id.to_string(),
            admin_id: admin_id.to_string(),
            creation_date: Utc::now(),
        }
    }
}

#[derive(
    Serialize, Debug, Clone, PartialEq, Insertable, Queryable, Identifiable, AsChangeset, JsonSchema,
)]
#[primary_key(poll_id)]
#[table_name = "polls"]
#[serde(rename_all = "camelCase")]
pub struct Poll {
    /// An unique string, which is the key used in querying for this poll.
    pub poll_id: String,
    /// The title of the poll
    pub title: String,
    /// The description of the poll
    pub instruction: String,
    /// The interval between [DateTime]s, in minutes.
    /// MAX: 32767
    pub datetime_interval: i16,
    /// The [DateTime<Utc>]s which users may vote on.
    pub offered_datetimes: Vec<DateTime<Utc>>,
    /// The end of the poll. After this point has passed, no new votes are allowed.
    pub deadline: DateTime<Utc>,
    /// The name of the theme, specific to this poll.
    pub theme: String,
}
impl Poll {
    pub fn new(poll_id: &str, setup_data: SetupData) -> Self {
        Self {
            poll_id: poll_id.to_string(),
            title: setup_data.title,
            instruction: setup_data.instruction,
            datetime_interval: setup_data.datetime_interval,
            offered_datetimes: setup_data.offered_datetimes,
            deadline: setup_data.deadline,
            theme: setup_data.theme,
        }
    }
}
