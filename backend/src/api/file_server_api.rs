//! The part of the API which serves static files, according to the provided path.

use crate::errors::ApiResult;

use rocket::{
    fs::NamedFile,
    get,
    http::uri::{fmt::Path as RocketPath, Segments},
    request::{FromParam, FromSegments},
};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use std::path::{Path, PathBuf};

pub const ROOT_ROUTE: &str = "../frontend/dist";
pub const ROOT_PAGES: [&str; 3] = ["index", "create", "about"];

/// Struct used to check whether the requested file has a file-extension.
/// Succeedes if there IS a file-extension (= a period).
#[derive(JsonSchema)]
pub struct PathWithFileExtension(PathBuf);

impl<'r> FromSegments<'r> for PathWithFileExtension {
    type Error = &'r str;

    fn from_segments(segments: Segments<'r, RocketPath>) -> Result<Self, Self::Error> {
        let path = PathBuf::from_segments(segments).map_err(|_| "Not a path.")?;

        match path.extension() {
            Some(_) => Ok(PathWithFileExtension(path)),
            None => Err("No file extension."),
        }
    }
}

/// Struct used to check whether the requested file has a file-extension.
/// Succeedes if there is NO file-extension and if the desired page actually
/// exists in root.
#[derive(JsonSchema)]
pub struct TruncatedRootHtml(PathBuf);

#[rocket::async_trait]
impl<'r> FromParam<'r> for TruncatedRootHtml {
    type Error = &'r str;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        let path = PathBuf::from_param(param).map_err(|_| "Not a path.")?;

        if path.extension().is_some() {
            return Err("File has a file-extension.");
        }

        match ROOT_PAGES.contains(&param) {
            true => Ok(TruncatedRootHtml(path)),
            false => Err("Not defined as a root-level html-file"),
        }
    }
}

// The actual API

/// Forwards root (`/`) to `some_path/index.html`.
#[openapi(tag = "File Server")]
#[get("/")]
pub(crate) async fn root_index() -> ApiResult<NamedFile> {
    let path = Path::new(ROOT_ROUTE).join("index.html");

    Ok(NamedFile::open(path).await?)
}

/// The "normal" file-server.
/// Used for all those requests which try to GET files that contain a file-extension.
/// Example: `main.css`
#[openapi(tag = "File Server")]
#[get("/<path..>", rank = 1)]
pub(crate) async fn file_server(path: PathWithFileExtension) -> ApiResult<NamedFile> {
    let path = Path::new(ROOT_ROUTE).join(path.0);

    Ok(NamedFile::open(path).await?)
}

/// A file-server for requests, that try to get html-files from root without mentioning the `.html`-part.
/// It does this by appending ".html" to the provided path.
///
/// Used to allow for loading of `website/about`, which is prettier than `website/about.html`.
#[openapi(tag = "File Server")]
#[get("/<file_name>", rank = 2)]
pub(crate) async fn root_add_html(file_name: TruncatedRootHtml) -> ApiResult<NamedFile> {
    let mut path = Path::new(ROOT_ROUTE).join(file_name.0);
    path.set_extension("html");

    Ok(NamedFile::open(path).await?)
}
