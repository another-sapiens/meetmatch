//! A collection of functions that handle web-requests.

mod dev_api;
pub mod file_server_api;
mod poll_api;

use dev_api::*;
use file_server_api::*;
use poll_api::*;

use rocket_okapi::openapi_get_routes;

pub fn routes() -> Vec<rocket::Route> {
    openapi_get_routes![
        // File-server-APIs
        root_index,
        file_server,
        root_add_html,
        // Poll- & vote-APIs
        new_poll,
        get_links,
        get_poll,
        get_poll_setupdata,
        update_poll,
        new_vote,
        get_vote,
        update_uservote,
        delete_vote,
        results,
        delete_poll,
        // Dev-APIs
        test,
        error,
        get_db,
    ]
}
