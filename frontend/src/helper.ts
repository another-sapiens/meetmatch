/** Turns a "string" into a "String", capitalizing the first character. */
function capitalize(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}

/** Returns an array of numbers, starting from `start` and ending with (but not including) `end`. */
function range(start: number, end: number): number[] {
    const rangeArray = <number[]>[];
    for (let i = start; i <= end; i++) {
        rangeArray.push(i);
    }
    return rangeArray;
}

/** Turns a number into a string and mkes sure the result has at least two digits. */
function addZero(nr: number): string {
    const formattedTime: string = nr < 10 ? `0${nr}` : nr.toString();
    return formattedTime;
}

/**
 * Returns an array of Dates.
 * The numbers start from `start` and end with (but do not include) `end`.
 */
function getDayRange(start: Date, end: Date) {
    const dates = <Date[]>[];
    const current = new Date(start);

    while (current < end) {
        dates.push(new Date(current));
        current.setDate(current.getDate() + 1);
    }
    return dates;
}

/**
 * Returns an array of numbers, representing the hours.
 * The numbers start from `startingHour` and end with (but do not include) `endingHour`.
 */
function getHourRange(
    startingHour: number,
    endingHour: number,
    interval: number
): number[] {
    if (interval < 60 * 24) {
        const hours = [];
        let currentHour = startingHour;

        while (currentHour < endingHour) {
            hours.push(currentHour);
            currentHour++;
        }
        return hours;
    } else {
        // If we want call this function but have an interval of `DAY`, only return "hour 0".
        return [0];
    }
}
/**
 * Returns an array of numbers, representing the minutes.
 * The numbers start from 0 and end with the last value that doesn't extend into the next hour.
 */
function getMinuteRange(interval: number): number[] {
    const minutes = [];
    let currentMinute = 0;

    while (currentMinute < 60) {
        minutes.push(currentMinute);
        currentMinute += interval;
    }
    return minutes;
}

/** Combines the dayte's year, month, and day with the provided hour and minute. */
function combineDayHourMinute(dayte: Date, hour: number, minute: number): Date {
    return new Date(
        dayte.getFullYear(),
        dayte.getMonth(),
        dayte.getDate(),
        hour,
        minute
    );
}

function copyTextToClipboard(text: string) {
    // Copy the URL in the input-field "linkP" to the clipboard.

    navigator.clipboard.writeText(text).then(
        function () {
            console.log("Async: Copying to clipboard was successful!");
        },
        function (err) {
            console.error("Async: Could not copy text: ", err);
        }
    );
}

export {
    capitalize,
    range,
    addZero,
    getDayRange,
    getHourRange,
    getMinuteRange,
    combineDayHourMinute,
    copyTextToClipboard,
};
